#!/usr/bin/env python

import rospy
from hypobot_system.srv import *

# Hardcoded and therefore will reset everytime system 
# is restarted. Future Work: Write out to DB
inventory = {
  "dextrose tablets": 100,
  "glucose gel": 100,
  "insulin pen": 100
}

def handle_reset_inventory(req):
    for key in inventory:
        inventory[key] = 1
    return GetInventoryResponse(str(inventory))

def handle_get_inventory(req):
    return GetInventoryResponse(str(inventory))

def handle_incr_inventory(req):
    inventory[req.payload] += req.count
    return UpdateInventoryResponse(str(inventory))

def handle_decr_inventory(req):
    inventory[req.payload] -= req.count
    return UpdateInventoryResponse(str(inventory))

def handle_remove_all_inventory(req):
    for key in inventory:
        inventory[key] = 0
    return GetInventoryResponse(str(inventory))

def get_inventory():
    s = rospy.Service('get_inventory', GetInventory, handle_get_inventory)

def incr_inventory():
    s = rospy.Service("incr_inventory", UpdateInventory, handle_incr_inventory)

def decr_inventory():
    s = rospy.Service("decr_inventory", UpdateInventory, handle_decr_inventory)

def reset_inventory():
    s = rospy.Service("reset_inventory", GetInventory, handle_reset_inventory)

def remove_all_inventory():
    s = rospy.Service("remove_all_inventory", GetInventory, handle_remove_all_inventory)

def create_services(robot_name):
    rospy.init_node('inventory_server')

    # available services
    get_inventory()
    incr_inventory()
    decr_inventory()
    reset_inventory()
    remove_all_inventory()
    
    rospy.loginfo("Inventory Server Running for %s", robot_name)
    rospy.spin()

if __name__ == "__main__":
    try:
       robot_name = rospy.get_param('robot_name')
       create_services(robot_name)
       rospy.spin()
    except rospy.ROSInterruptException:
        pass