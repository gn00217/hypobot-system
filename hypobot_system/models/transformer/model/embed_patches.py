import torch.nn as nn
import torch

class EmbedPatches(nn.Module):
    def __init__(self, img_size, patch_size, token, latent_vec_size=768):
        super(EmbedPatches, self).__init__()

        # Token - is the [cls] token being used?
        # [cls] token is an extra learnable embedding that encompasses the information of all patches
        # Input to the final MLP Head would typically be the output from this [cls] Token
        # an alt. method is to average the values from all output vectors instead, exc. [cls]
        # both options have been tested
        self.token = token

        self.no_patches = (img_size // patch_size) ** 2
        self.project = nn.Conv2d(
                3, # RGB - Channels
                latent_vec_size,
                kernel_size=patch_size,
                stride=patch_size,
        ) # configured patches to have no overlap

        if self.token:
            self.cls_token = nn.Parameter(torch.zeros(1, 1, latent_vec_size))
            # learn the positions of all patches including [cls]
            self.patch_position_embed = nn.Parameter(torch.zeros(1, 1 + self.no_patches, latent_vec_size))
        else:
            self.patch_position_embed = nn.Parameter(torch.zeros(1, self.no_patches, latent_vec_size))

        self.dropout = nn.Dropout(p=0.1)

    def forward(self, x):
        batch_size = x.shape[0]
        if self.token:
            cls_token = self.cls_token.expand(batch_size, -1, -1) # updating cls_token dims w/ batch_size
            x = self.project_dim(x)
            x = torch.cat((cls_token, x), dim=1) # add [cls] token as a patch
        else:
            x = self.project_dim(x)

        x = x + self.patch_position_embed # adding patch values embedding w/ positions of patches
        x = self.dropout(x)
        return x

    # projects patches into latent vector size dimensions
    def project_dim(self, x):
        x = self.project(x)
        x = x.flatten(2)
        x = x.transpose(1, 2)
        return x
