import torch.nn as nn

from multihead_self_attention import MHSA
from mlp import MLP

# See diagram on pg3 TransformerEncoder https://arxiv.org/pdf/2010.11929.pdf
class TransformerEncoder(nn.Module):
    def __init__(self, latent_vec_size, no_heads):
        super(TransformerEncoder, self).__init__()

        self.encoder_block_1 = nn.Sequential(
                                nn.LayerNorm(latent_vec_size),
                                MHSA(latent_vec_size,no_heads=no_heads)
                                )
        # mlp ratio of 4 used to decide features the output dim of first
        # linear layer in mlp - not sure why - nothing in paper on this
        # see links to reference materials
        mid_dim = int(latent_vec_size * 4.0)
        self.encoder_block_2 = nn.Sequential(
                                    nn.LayerNorm(latent_vec_size),
                                    MLP(
                                        input_dim=latent_vec_size,
                                        mid_dim=mid_dim,
                                        out_dim=latent_vec_size,
                                    ))

    def forward(self, x):
        x = x + self.encoder_block_1(x)
        x = x + self.encoder_block_2(x)
        return x
