import torch.nn as nn
import torch
import numpy as np
import torch.nn.functional as F

class MHSA(nn.Module):
    def __init__(self, latent_vec_size, no_heads=3):
        super(MHSA, self).__init__()
        self.no_heads = no_heads # how many individual heads in msa
        self.head_dim = latent_vec_size // no_heads # when heads outputs are concat they have same dim as input

        self.qkv = nn.Linear(latent_vec_size, latent_vec_size*3, bias=False)
        self.attn_dropout = nn.Dropout(0.1)
        self.project = nn.Linear(latent_vec_size,latent_vec_size)

    def getQKV(self, x, batch_size, no_seq, latent_vec_size):
        x = x.reshape(batch_size, no_seq, self.no_heads, 3*self.head_dim) # (batch_size, no_seq, no_heads, qkv-values)
        x = x.permute(0,2,1,3) # (batch_size, no_heads, no_seq, qkv-values)
        return x.chunk(3, dim=-1) # splits q,k,v values

    def calculateAttentions(self, q, k, v):
        softmax_norm = self.head_dim**-0.5
        input_to = torch.matmul(q, k.transpose(-2,-1)) * softmax_norm
        return F.softmax(input_to, dim=-1) # returns prob dist. to use as weightings

    # https://arxiv.org/pdf/2010.11929.pdf - Appendix A eq 5-7
    def forward(self, x):
        # embedded input patches run through a linear layer to learn
        # corresponding weights for key, value and query
        # output is the key, value, query
        batch_size, no_seq, latent_vec_size = x.shape # (batch_size, no_seq, latent_vec_size)
        qkv = self.qkv(x) # (batch_size, no_seq, latent_vec_size*3)
        q,k,v = self.getQKV(qkv, batch_size, no_seq, latent_vec_size)

        attentions = self.calculateAttentions(q,k,v)
        attentions = self.attn_dropout(attentions)
        self_attention = torch.matmul(attentions, v) # (batch_size, no_heads, no_seq, head_dim)
        self_attention = self_attention.transpose(1,2) # (batch_size, no_seq, no_heads, head_dim)
        self_attention = self_attention.flatten(2) # concats values from all the no_heads

        context_vectors = self.project(self_attention)
        return context_vectors
