import torch.nn as nn

class MLP(nn.Module):
    def __init__(self, input_dim, mid_dim, out_dim):
        super(MLP, self).__init__()
        self.mlp = nn.Sequential(
                            nn.Linear(input_dim, mid_dim),
                            nn.GELU(),
                            nn.Linear(mid_dim, out_dim),
                            nn.Dropout(0.1))

    def forward(self, x):
        return self.mlp(x)
