from embed_patches import EmbedPatches
from transformer_encoder import TransformerEncoder
import torch.nn as nn
import torch

class VIT(nn.Module):
    def __init__(self, img_size, patch_size, latent_vec_size=768, no_encoders=3, no_heads=3, type="regression", token=True, no_classes=4):
            super(VIT, self).__init__()
            self.token = token

            # regression or classification - Note: Classification is untested
            # in this implementation
            self.type = type
            self.embed_patches = EmbedPatches(
                    img_size=img_size,
                    patch_size=patch_size,
                    latent_vec_size=latent_vec_size,
                    token=token
            )
            self.encoders = nn.ModuleList(
                [
                    TransformerEncoder(
                        latent_vec_size=latent_vec_size,
                        no_heads=no_heads
                    )
                    for _ in range(no_encoders)
                ]
            )

            if type == "regression":
                self.regress_fc_pose = nn.Sequential(nn.Linear(latent_vec_size, 2048),
                                                     nn.ReLU(),
                                                     nn.Dropout(0.5))
                self.fc_xyz = nn.Linear(2048, 3)
                self.fc_wpqr = nn.Linear(2048, 4)
            else:
                self.fc_class = nn.Linear(latent_vec_size, no_classes)

    def forward(self, x):
            x = self.embed_patches(x)
            for encoder in self.encoders:
                x = encoder(x)

            if self.token:
                final_output = x[:, 0] # output of class_token
            else:
                final_output = torch.mean(x, 1) # alt. method - avg of all patch outputs

            if self.type == 'regression':
                x = self.regress_fc_pose(final_output)
                xyz = self.fc_xyz(x)
                ori = self.fc_wpqr(x)

                return xyz, ori

            else:
                return self.fc_class(final_output)
