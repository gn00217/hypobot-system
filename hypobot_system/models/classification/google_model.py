import torch
import torch.nn as nn
import matplotlib.pyplot as plt
import torch.optim as optim
import numpy as np

# https://arxiv.org/pdf/1409.4842.pdf

class GoogLeNet(nn.Module):
    def __init__(self, in_channels=3, classes=4):
        super(GoogLeNet, self).__init__()

        self.conv1 = ConvBlock(in_channels=in_channels, out_channels=64, kernel_size=7, stride=2, padding=3)
        self.maxpool1 = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.conv2 =  nn.Sequential(
            ConvBlock(64, 64, kernel_size=1, stride=1, padding=0),
            ConvBlock(64, 192, kernel_size=3, stride=1, padding=1)
        )

        self.maxpool2 = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

        self.incept3a = InceptionBlock(192,64,96,128,16,32,32)
        self.incept3b = InceptionBlock(256,128,128,192,32,96,64)
        self.maxpool3 = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

        self.incept4a = InceptionBlock(480,192,96,208,16,48,64)
        self.aux1 = AuxBlock(512, classes)
        self.aux2 = AuxBlock(528, classes)

        self.incept4b = InceptionBlock(512,160,112,224,24,64,64)
        self.incept4c = InceptionBlock(512,128,128,256,24,64,64)
        self.incept4d = InceptionBlock(512,112,144,288,32,64,64)
        self.incept4e = InceptionBlock(528,256,160,320,32,128,128)
        self.maxpool4 = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

        self.incept5a = InceptionBlock(832,256,160,320,32,128,128)
        self.incept5b = InceptionBlock(832,384,192,384,48,128,128)

        self.avgpool = nn.AvgPool2d(kernel_size=7, stride=1)
        self.dropout = nn.Dropout(p=0.4)
        self.fc1 = nn.Linear(1024, 4)

    def forward(self, x):
        x = self.conv1(x)
        x = self.maxpool1(x)
        x = self.conv2(x)
        x = self.maxpool2(x)

        x = self.incept3a(x)
        x = self.incept3b(x)
        x = self.maxpool3(x)

        x = self.incept4a(x)

        aux_out1 = self.aux1(x)

        x = self.incept4b(x)
        x = self.incept4c(x)
        x = self.incept4d(x)

        aux_out2 = self.aux2(x)

        x = self.incept4e(x)
        x = self.maxpool4(x)
        x = self.incept5a(x)
        x = self.incept5b(x)
        x = self.avgpool(x)
        x = x.reshape(x.shape[0], -1)

        x = self.dropout(x)
        x = self.fc1(x)

        return x, aux_out1, aux_out2

class AuxBlock(nn.Module):
    def __init__(self, x, classes):
        super(AuxBlock, self).__init__()
        self.avgpool2 = nn.AvgPool2d(stride=3, kernel_size=5)
        self.conv3 = ConvBlock(x, 128, kernel_size=1, stride=1)
        self.relu = nn.ReLU()
        self.fc = nn.Linear(4 * 4 * 128, 1024)
        self.dropout = nn.Dropout(p=0.7)
        self.classifier = nn.Linear(1024, classes)

    def forward(self,input):
        N = input.shape[0]
        x = self.avgpool2(input)
        x = self.conv3(x)
        x = self.relu(x)
        x = x.reshape(N, -1)
        x = self.fc(x)
        x = self.dropout(x)
        x = self.classifier(x)

        return x

class InceptionBlock(nn.Module):
    def __init__(self, in_channels, post_1x1, pre_3x3, post_3x3, pre_5x5, post_5x5, post_pool):
        super(InceptionBlock, self).__init__()

        self.branch1 = ConvBlock(in_channels, post_1x1, kernel_size=1)
        self.branch2 = nn.Sequential(
            ConvBlock(in_channels, pre_3x3, kernel_size=1),
            ConvBlock(pre_3x3, post_3x3, kernel_size=3, padding=1)
        )
        self.branch3 = nn.Sequential(
            ConvBlock(in_channels, pre_5x5, kernel_size=1),
            ConvBlock(pre_5x5, post_5x5, kernel_size=5, padding=2)
        )
        self.branch4 = nn.Sequential(
            nn.MaxPool2d(kernel_size=3, stride=1, padding=1),
            ConvBlock(in_channels, post_pool, kernel_size=1)
        )

    def forward(self, x):
        return torch.cat([self.branch1(x), self.branch2(x), self.branch3(x), self.branch4(x)],1)

class ConvBlock(nn.Module):
    def __init__(self, in_channels, out_channels, **kwargs):
        super(ConvBlock, self).__init__()
        self.relu = nn.ReLU()
        self.conv = nn.Conv2d(in_channels, out_channels, **kwargs)
        # https://arxiv.org/abs/1502.03167
        self.bnorm = nn.BatchNorm2d(out_channels)

    def forward(self, x):
        x = self.conv(x)
        x = self.bnorm(x)
        return self.relu(x)

if __name__ == '__main__':    
    GoogLeNet()