#!/usr/bin/env python

import torch
import numpy as np

import rospy
import tf
from geometry_msgs.msg import PoseStamped
import sys

sys.path.insert(1, "/home/george/aloe-catkin/src/hypobot_system/models/regression")

from data_loader import get_loader
from model import model_parser

class FindViaRegression():
    def __init__(self):
        self.model = model_parser('Resnet', False, 0.5, False)

    def find_location(self):
        self.data_loader = get_loader('Resnet', '/home/george/aloe-catkin/src/hypobot_system', '/home/george/aloe-catkin/src/hypobot_system/models/regression/dummy_metadata.csv', 'test', 'True')
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

        self.model = self.model.to(self.device)
        self.model.load_state_dict(torch.load("/home/george/aloe-catkin/src/hypobot_system/models/regression/150_best_net.pth"))
        self.model.eval()

        pose = np.zeros((1,7))
        with torch.no_grad():
            for i, (inputs,poses) in enumerate(self.data_loader):
                pos_out, ori_out, _ = self.model(inputs)
                pose = torch.cat((pos_out, ori_out), dim=1)
                pose = pose.numpy()
                new_pose = self.transform(pose[0])
                print(new_pose)
                return new_pose
                

    def transformPoint(self,x,y,z,rx,ry,rz,rw, listener):
        ps = PoseStamped()
        ps.header.frame_id = "/butthead/map-gt"
        ps.header.stamp = rospy.Time(0)
        ps.pose.position.x = x
        ps.pose.position.y = y
        ps.pose.position.z = z
        ps.pose.orientation.x = rx
        ps.pose.orientation.y = ry
        ps.pose.orientation.z = rz
        ps.pose.orientation.w = rw    
        new_pose = listener.transformPose('/butthead/map', ps)

        return new_pose.pose.position.x, new_pose.pose.position.y

    def transform(self, pose):
        listener = tf.TransformListener()
        x,y,z,rx,ry,rz,rw = pose[:]
        listener.waitForTransform('/butthead/map', '/butthead/map-gt', rospy.Time(), rospy.Duration(5.0))
        new_x, new_y = self.transformPoint(x,y,z,rx,ry,rz,rw, listener)
        return new_x,new_y

def main():
  FindViaRegression()

if __name__ == '__main__':
    main()