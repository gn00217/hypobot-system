import rospy
from manage_inventory import ManageInventory
from navigate import Navigate

class Robot():
    def __init__(self):
        self.name = None
        self.inv_manager = None
        self.nav_controller = None
        self.attending_event = False

class RobotManager():
    def __init__(self):
        self.online_robots = rospy.get_param('online_robots')
        self.num_online_robots = len(self.online_robots)
        if len(self.online_robots) == 0:
            rospy.logerr("FAILURE: No Robots Configured - Check parameters")
        else:
            self.agent_1 = Robot()
            self.agent_1.name = self.online_robots[0]
            self.agent_1.inv_manager = ManageInventory(self.agent_1.name)
            self.agent_1.nav_controller = Navigate(self.agent_1.name)
            rospy.loginfo("{0} is ready".format(self.agent_1.name))

        if len(self.online_robots) == 2:      
            self.agent_2 = Robot()
            self.agent_2.name = self.online_robots[1]
            self.agent_2.inv_manager = ManageInventory(self.agent_2.name)
            self.agent_2.nav_controller = Navigate(self.agent_2.name)
            rospy.loginfo("{0} is ready".format(self.agent_2.name))
    
    # no. robots available i.e those not attending events
    def number_robots_available(self):
        if len(self.online_robots) > 1:
            return int(not self.agent_1.attending_event) + int(not self.agent_2.attending_event)
        else:
            return int(not self.agent_1.attending_event)
    
    # validates for whether this is a single or multiple robot supported system
    # then returns the available robot
    def get_available_robot(self):
        if len(self.online_robots) > 1:
            if self.agent_1.attending_event == False:
                return self.agent_1
            else:
                return self.agent_2
        else:
            return self.agent_1
    
    # only applies to multiple robot support system
    def get_available_robots(self):
        return self.agent_1, self.agent_2


    