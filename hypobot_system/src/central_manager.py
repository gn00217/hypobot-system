#!/usr/bin/env python

from find_location_via_reg import FindViaRegression
import rospy
from register_manager import RobotManager
from events import Events
import cv2
from cv_bridge import CvBridge
from hypobot_system.msg import event_via_pose, event_via_img
from find_location_via_class import FindViaClassification
from find_location_via_reg import FindViaRegression
from find_location_via_tf import FindViaTransformer

# TODO: Clean up allocation and use Events Class for control of prioritistion
class CentralManager():
    def __init__(self):
       # register robots - assigns them an inventory server and navigation ability
       self.rm = RobotManager()
       self.events = Events()
       self.bridge = CvBridge()
       self.search_method = rospy.get_param('location_method') # pose provided, req. regression or classification
       self.find_class = FindViaClassification()
       self.find_poser = FindViaRegression()
       self.find_poset = FindViaTransformer()
       self.event_type = None # message type
       self.monitor_events()
    
       self.restock_goal = event_via_pose()
       self.restock_goal.position.position.x = rospy.get_param('restock_loc_x')
       self.restock_goal.position.position.y = rospy.get_param('restock_loc_y')
       self.restock_goal.position.orientation.w = 1

    def find_location(self, image):
        img = self.bridge.imgmsg_to_cv2(image)
        cv2.imwrite('/home/george/aloe-catkin/src/hypobot_system/location.png', img)

        if self.search_method == 'classification':
            return self.find_class.get_location()
        if self.search_method == 'regression':
            return self.find_poser.find_location()
        if self.search_method == 'transformer':
            return self.find_poset.find_location()

    def allocate_rescue_robot(self, msg):
        if self.rm.number_robots_available() == 1:
            robot = self.rm.get_available_robot()
            robot.attending_event = True
            if robot.inv_manager.has_inventory(msg.payload)[0]:
                rospy.loginfo("{0} is navigating to the rescue location".format(robot.name))
                ######################### for test only
                print(robot.nav_controller.calculate_dist(msg))
                ######################################
                state = robot.nav_controller.send_goal(msg)
                rospy.loginfo("{0} has reached the rescue location".format(robot.name))
                if state == 3:
                    ######################### for test only
                    print(rospy.get_rostime())
                    ######################################
                    robot.inv_manager.decrease_inventory(msg.payload)
                    robot.attending_event = False
            else:
                rospy.loginfo("{0} is navigating to the restock location prior to rescue location".format(robot.name))
                robot.nav_controller.send_goal(self.restock_goal)
                robot.inv_manager.increase_inventory()
                state = robot.nav_controller.send_goal(msg)
                if state == 3:
                    robot.inv_manager.decrease_inventory(msg.payload)
                    robot.attending_event = False

        # allocate
        if self.rm.number_robots_available() > 1:
            robot_1, robot_2 = self.rm.get_available_robots()
            if robot_1.inv_manager.has_inventory(msg.payload)[0] and robot_2.inv_manager.has_inventory(msg.payload)[0]:
                if robot_1.nav_controller.calculate_dist(msg) < robot_2.nav_controller.calculate_dist(msg):
                    robot_1.attending_event = True
                    rospy.loginfo("{0} is navigating to the rescue location".format(robot_1.name))
                    state = robot_1.nav_controller.send_goal(msg)
                    if state == 3:
                        robot_1.inv_manager.decrease_inventory(msg.payload)
                        robot_1.attending_event = False
                else:
                    robot_2.attending_event = True
                    rospy.loginfo("{0} is navigating to the rescue location".format(robot_2.name))
                    state = robot_2.nav_controller.send_goal(msg)
                    if state == 3:
                        robot_2.inv_manager.decrease_inventory(msg.payload)
                        robot_2.attending_event = False
        
            elif robot_1.inv_manager.has_inventory(msg.payload)[0] and not robot_2.inv_manager.has_inventory(msg.payload)[0]:
                robot_1.attending_event = True
                rospy.loginfo("{0} is navigating to the rescue location".format(robot_1.name))
                state = robot_1.nav_controller.send_goal(msg)
                if state == 3:
                    robot_1.inv_manager.decrease_inventory(msg.payload)
                    robot_1.attending_event = False
            elif not robot_1.inv_manager.has_inventory(msg.payload)[0] and robot_2.inv_manager.has_inventory(msg.payload)[0]:
                robot_2.attending_event = True
                rospy.loginfo("{0} is navigating to the rescue location".format(robot_2.name))
                state = robot_2.nav_controller.send_goal(msg)
                if state == 3:
                    robot_2.inv_manager.decrease_inventory(msg.payload)
                    robot_2.attending_event = False
            else:
                if robot_1.nav_controller.calculate_dist(self.restock_goal) < robot_2.nav_controller.calculate_dist(self.restock_goal):
                    robot_1.attending_event = True
                    rospy.loginfo("{0} is navigating to the restock location prior to rescue location".format(robot_1.name))
                    state = robot_1.nav_controller.send_goal(self.restock_goal)
                    if state == 3:
                        robot_1.inv_manager.increase_inventory()
                        new_state = robot_1.nav_controller.send_goal(msg)
                        if new_state == 3:
                            robot_1.inv_manager.decrease_inventory(msg.payload)
                            robot_1.attending_event = False
                else:
                    robot_2.attending_event = True
                    rospy.loginfo("{0} is navigating to the restock location prior to rescue location".format(robot_2.name))
                    state = robot_2.nav_controller.send_goal(self.restock_goal)
                    if state == 3:
                        robot_2.inv_manager.increase_inventory()
                        new_state = robot_2.nav_controller.send_goal(msg)
                        if new_state == 3:
                            robot_2.inv_manager.decrease_inventory(msg.payload)
                            robot_2.attending_event = False

    def handle_event(self, msg):
        if self.event_type == event_via_img:
            x, y = self.find_location(msg.image)
            new_msg = event_via_pose()
            new_msg.position.position.x = x
            new_msg.position.position.y = y
            new_msg.position.orientation.w = 1
            new_msg.payload = msg.payload
            self.allocate_rescue_robot(new_msg)
        else:
            rospy.loginfo("Rescue event received - Action is being taken")
            self.allocate_rescue_robot(msg)

    def red_callback(self, msg):
        if self.rm.number_robots_available() == 0:
            self.events.red_events.append(msg)
            rospy.loginfo("No robots available - Event has been queued as high priority")
        else:
            self.handle_event(msg)

    def amber_callback(self, msg):
        if self.rm.number_robots_available() == 0:
            self.events.amber_events.append(msg)
            rospy.loginfo("No robots available - Event has been queued as medium priority")
        else:
            print(rospy.get_rostime())
            self.handle_event(msg)

    def green_callback(self, msg):
        if self.rm.number_robots_available() == 0:
            self.events.green_events.append(msg)
            rospy.loginfo("No robots available - Event has been queued as low priority")
        else:
            self.handle_event(msg)
        
    def monitor_events(self):
        rospy.loginfo("Location search via {0}".format(self.search_method))
        if self.search_method == 'pose':
            self.event_type = event_via_pose
        else:
            self.event_type = event_via_img
        rospy.Subscriber('red_events', self.event_type, self.red_callback)
        rospy.Subscriber('amber_events', self.event_type, self.amber_callback)
        rospy.Subscriber('green_events', self.event_type, self.green_callback)


if __name__ == '__main__':
    rospy.init_node('central_manager')
    CentralManager()
    try:
       rospy.spin()

    except rospy.ROSInterruptException:
        pass