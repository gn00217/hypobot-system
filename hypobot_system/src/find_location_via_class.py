#!/usr/bin/env python

import torch
import torch.nn as nn
import torchvision.transforms as transforms
from PIL import Image
import sys

sys.path.insert(1, "/home/george/aloe-catkin/src/hypobot_system/models/classification")

from google_model import GoogLeNet
import torchvision.transforms as transforms

class FindViaClassification():
    def __init__(self):
        self.model = GoogLeNet()

    def get_location(self):
        self.model.load_state_dict(torch.load('/home/george/aloe-catkin/src/hypobot_system/models/classification/best_cls_net.pth', map_location='cpu'))

        map_label = {
            0: (40.9, 38.5), #'Robot Lab',
            1: (40.5, 40), #'Stationary Room',
            2: (41.4, 30.8), #'Corridor',
            3: (39, 26) #'PhD Lab'
        }
        room_label = {
            0: 'Robot Lab',
            1: 'Stationary Room',
            2: 'Corridor',
            3: 'PhD Lab'
        }

        image = Image.open('/home/george/aloe-catkin/src/hypobot_system/location.png')
        softmax = nn.Softmax(dim=1)
        convert_tensor = transforms.Compose([transforms.CenterCrop(224),transforms.ToTensor()])
        image = convert_tensor(image)
        image = image.unsqueeze(0)
        
        with torch.no_grad():
            output, o2, o3 = self.model(image)
            output = softmax(output)
            index = torch.argmax(output)
        
            label = map_label[index.item()]
            print(room_label[index.item()])
            print(label)

        return label

def main():
  FindViaClassification()

if __name__ == '__main__':
    main()