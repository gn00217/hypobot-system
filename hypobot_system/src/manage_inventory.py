import rospy
from mini_delivery.srv import *

class ManageInventory():
    def __init__(self, robot_name):
        self.robot_name = robot_name
        self.get_inventory = rospy.ServiceProxy('{0}/get_inventory'.format(robot_name), GetInventory)
        self.incr_inventory = rospy.ServiceProxy('{0}/incr_inventory'.format(robot_name), UpdateInventory)
        self.decr_inventory = rospy.ServiceProxy('{0}/decr_inventory'.format(robot_name), UpdateInventory)
    
    def has_inventory(self, req_payload):
        rospy.wait_for_service('{0}/get_inventory'.format(self.robot_name))
        try:
            inventory = self.get_inventory()
            inventory = eval(inventory.available_inventory)
            return (True if  inventory[req_payload] > 0 else False), inventory
        
        except rospy.ServiceException as e:
            self.handle_service_failure(e)
    
    def increase_inventory(self):
        rospy.wait_for_service('{0}/get_inventory'.format(self.robot_name))
        try:
            inventory = self.get_inventory()
            inventory = eval(inventory.available_inventory)
        except rospy.ServiceException as e:
            self.handle_service_failure(e)
            return False

        listOfKeys = [key for (key, value) in inventory.items() if value == 0]
        rospy.wait_for_service('{0}/incr_inventory'.format(self.robot_name))
        try:
            for item in listOfKeys:
               self.incr_inventory(item, 1)
            return True
        except rospy.ServiceException as e:
            self.handle_service_failure(e)
            return False

    def decrease_inventory(self, payload):
        rospy.wait_for_service('{0}/decr_inventory'.format(self.robot_name))
        try:
            self.decr_inventory(payload, 1)
        except rospy.ServiceException as e:
            self.handle_service_failure(e)

    def handle_service_failure(self, e):
        rospy.logerr("Service call failed: %s", e)
        pass