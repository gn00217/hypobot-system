#!/usr/bin/env python

import torch
import torch.nn as nn
import torchvision.transforms as transforms
from PIL import Image
import sys
import rospy
import tf
from geometry_msgs.msg import PoseStamped

sys.path.insert(1, "/home/george/aloe-catkin/src/hypobot_system/models/transformer/model")

from vit import VIT

class FindViaTransformer():
    def __init__(self):
        self.model = VIT(img_size=256, patch_size=32, token=True)

    def find_location(self):
        self.model.load_state_dict(torch.load('/home/george/aloe-catkin/src/hypobot_system/models/transformer/best_cls_net.pth', map_location='cpu')['model_state_dict'])

        image = Image.open('/home/george/aloe-catkin/src/hypobot_system/location.png')
        convert_tensor = transforms.Compose([transforms.CenterCrop(256),transforms.ToTensor()])
        image = convert_tensor(image)
        image = image.unsqueeze(0)
        print(image.shape)        

        with torch.no_grad():
            pos, ori = self.model(image)
            pose = torch.cat((pos, ori),1)
            new_pose = self.transform(pose)
            print(new_pose)

        return new_pose

    def transformPoint(self,x,y,z,rx,ry,rz,rw, listener):
        ps = PoseStamped()
        ps.header.frame_id = "/butthead/map-gt"
        ps.header.stamp = rospy.Time(0)
        ps.pose.position.x = x
        ps.pose.position.y = y
        ps.pose.position.z = z
        ps.pose.orientation.x = rx
        ps.pose.orientation.y = ry
        ps.pose.orientation.z = rz
        ps.pose.orientation.w = rw    
        new_pose = listener.transformPose('/butthead/map', ps)

        return new_pose.pose.position.x, new_pose.pose.position.y

    def transform(self, pose):
        listener = tf.TransformListener()
        x,y,z,rx,ry,rz,rw = pose[0]
        listener.waitForTransform('/butthead/map', '/butthead/map-gt', rospy.Time(), rospy.Duration(5.0))
        new_x, new_y = self.transformPoint(x,y,z,rx,ry,rz,rw, listener)
        return new_x,new_y

def main():
  FindViaTransformer()

if __name__ == '__main__':
    main()