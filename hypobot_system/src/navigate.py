import rospy
import actionlib
import math
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import Pose, Point, Quaternion, PoseStamped, PoseWithCovarianceStamped
from nav_msgs.srv import GetPlan

class Navigate():
    def __init__(self, robot_name):
        self.robot_name = robot_name
        self.client = actionlib.SimpleActionClient('/{0}/move_base'.format(self.robot_name), MoveBaseAction)
        self.make_plan = rospy.ServiceProxy("/{0}/move_base/make_plan".format(self.robot_name), GetPlan)
        self.pose_sub = rospy.Subscriber("/{0}/amcl_pose".format(self.robot_name), PoseWithCovarianceStamped, self.update_current_pos)

        # TODO: Use the Pose type
        self.current_pose = { # estimated current pose from amcl
            'x': None,
            'y': None
        }

        wait = self.client.wait_for_server(rospy.Duration(60))
        if not wait:
            rospy.logerr("Action server not available!")
            rospy.signal_shutdown("Action server not available!")
            return

        rospy.loginfo("Connected to Action server for %s", robot_name)

    def update_current_pos(self, result):
        self.current_pose = {
            'x':result.pose.pose.position.x,
            'y': result.pose.pose.position.y
        }

    def send_goal(self, msg):
        self.client.wait_for_server()
        goal = MoveBaseGoal()
        map = "/{0}/map".format(self.robot_name)
        goal.target_pose.header.frame_id = map
        goal.target_pose.header.stamp = rospy.Time.now()
        goal.target_pose.pose = msg.position
        self.client.send_goal(goal)
        wait = self.client.wait_for_result(rospy.Duration(200))
        if not wait:
            rospy.logerr("Action server not available!")
        else:
            return self.client.get_state()
    
    def get_plan(self, event):
        start = PoseStamped()
        map = "{0}/map".format(self.robot_name)
        start.header.seq = 0
        start.header.frame_id = map
        start.header.stamp = rospy.Time(0)

        start.pose.position.x = self.current_pose['x']
        start.pose.position.y = self.current_pose['y']

        Goal = PoseStamped()
        Goal.header.seq = 0
        Goal.header.frame_id = map
        Goal.header.stamp = rospy.Time.now()
        Goal.pose.position.x = event['position']['x']
        Goal.pose.position.y = event['position']['y']

        srv = GetPlan()
        srv.start = start
        srv.goal = Goal
        srv.tolerance = 1.5
        
        service = "/{0}/move_base/make_plan".format(self.robot_name)
        try:
            rospy.wait_for_service(service, 2)
            plan = self.make_plan(srv.start, srv.goal, srv.tolerance)
            return plan
        except rospy.ServiceException:
            pass
    
    # L2 Error
    def distance_between_pose(self, pose1, pose2):
        return math.sqrt(pow(pose2.position.x-pose1.position.x, 2) +
                         pow(pose2.position.y-pose1.position.y, 2))

    def compute_path_length(self, plan):
        poses = plan.poses
        pathLength = 0
        #Iteration among along the poses in order to compute the length
        for index in range(1, len(poses)):
            pathLength += self.distance_between_pose(poses[index-1].pose, poses[index].pose)
        rospy.loginfo("Path Length %s", str(pathLength))
        return pathLength
    
    def calculate_dist(self, msg):
        msg = {
            'position': {
                'x': msg.position.position.x,
                'y': msg.position.position.y
                }
         }
        
        plan = self.get_plan(msg)
        return self.compute_path_length(plan.plan)
        

   