# HypoBot: Robotic Navigation and Exploration for Diabetes Assistance

In the UK alone in 2015, 1182 paramedics were called to treat hypoglycaemic attacks [1],
one of the most common complications of diabetes, which affects 422 million people
world wide [2]. Advanced age increases the risk of these attacks, which are more likely to
result in hospitalisation. There is a scarce number of robotic-based solutions that aim to
solve problems faced by Diabetics; those that do exist are mainly invasive or proactive in
nature but do not eradicate the problem of hypoglycaemic attacks. This dissertation
introduces a reactive solution, HypoBot; a system that integrates multiple mobile robots to
deliver a self-administrable medical payload to an individual suffering from a
hypoglycaemic attack. The aim of this solution is to not only aid in an individual's
recovery but to also reduce the stress on caregivers and lower the number of paramedic
call-outs. A proof of concept of the HypoBot system was developed integrating multiple
TurtleBot2’s in simulation. A single mobile robot was integrated into the HypoBot system
on the real hardware and ran successfully. The system is able to respond to mock
hypoglycaemia events by evaluating which robots in the system would be the optimal
choice to deliver the payload directly to the user. Furthermore, the system allows each
robot to manage its own inventory. Multiple best suited approaches to localising a patient,
as found by a critical analysis of potential solutions including a novel end to end
transformer absolute pose regression model, were implemented into the HypoBot system.
Results show that the application of the HypoBot system is feasible provided some
changes are made such as varying the location of the robots base and finding an alternative
method in deciding which event should be attended to first.

## Support

*ROS: Melodic
*Python: 2.7

## Getting started

1. Create a Catkin Workspace - http://wiki.ros.org/catkin/Tutorials/create_a_workspace
2. Clone this project and cvssp_turtlebot_multiple_sim


## Running the Simulator


  ```roslaunch turtlebot_gazebo turtlebot_world.launch```

  ```roslaunch turtlebot_gazebo amcl_demo.launch```

  ```roslaunch turtlebot_rviz_launchers view_navigation.launch```

### Known Issue

If you get this message ```Invalid roslaunch XML syntax: [Errno 2] No such file or directory: u'/home/george/catkin_ws/src/cvssp_turtlebot_multiple_sim/launch/multiple/includes/amcl/asus_xtion_pro_amcl.launch.xml'```

Run: ```export TURTLEBOT_3D_SENSOR=kinect```


## Running HypoBot in Simulator

 **Single Robot**

  ```roslaunch single_bot_respond_to_event.launch```
  
  **Two Robots**
  
  ```roslaunch multiple_bot_respond_to_event.launch```
    
  <ins> Ensure that the correct name and number of robots are reflected in the available_robots config before running (located in ```config/available_robots.yaml```) </ins>

  
  ### Additional Params
  
  There are a few params you can change in the /param directory
  
  *Environment Restocking Location*
  1. restock_loc_x - float x-position where the robot will restock its inventory
  2. restock_loc_y - float y-position where the robot will restock its inventory

  *Robots Online*
  1. online_robots - list of the robots and working in the hypobot system and their names

  *System Method of Patient Localisation*
  1. location_method - 'pose' is the default option where you will have to provide the system with the 2D pose of the patient, other options, via CNN classification, CNN Regressor, Transformer Regressor will require an image to find the patient

  ## Sending an event to the system

  ### Manually for 'Pose' option

  Position - x,y within your map
  Payload - 'insulin pen', 'glucose gel' or 'dextrose tablets'
  Urgency - 'red', 'amber' or 'green'

